﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IteratorDesignPattern
{
    public interface IRegisterVehicule
    {
        void AddVehicule(string brand, string model, double price);
        Vehicule DisplayVehiculeInformation(int index);
        IIteratorVehicule GetIterator();
    }
}
