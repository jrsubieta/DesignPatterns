﻿using System;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IteratorDesignPattern
{
    public class RegisterVehicules : IRegisterVehicule
    {
        private ArrayList listVehicules;

        public RegisterVehicules()
        {
            this.listVehicules = new ArrayList();
        }

        public void AddVehicule(string brand, string model, double price)
        {
            Vehicule v = new Vehicule(brand, model, DateTime.Now, price);
            listVehicules.Add(v);
        }
        public Vehicule DisplayVehiculeInformation(int index)
        {
            return (Vehicule)listVehicules[index];
        }

        public IIteratorVehicule GetIterator()
        {
            return new IteratorVehicule(listVehicules);
        }
    }
}
