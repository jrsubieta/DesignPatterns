﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IteratorDesignPattern
{
    public class Vehicule
    {
        public string Brand { get; set; }
        public string Model { get; set; }
        public DateTime FactoryDate { get; set; }
        public double Price { get; set; }

        public Vehicule(string brand, string model, DateTime fdate, double price)
        {
            this.Brand = brand;
            this.Model = model;
            this.FactoryDate = fdate;
            this.Price = price;
        }

        public string FeaturesVehicule()
        {
            return Brand + " " + Model + " made up at " +
                FactoryDate.ToShortDateString() + "with a cost of " +
                Price + " euros.\n";
        }
    }
}
