﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IteratorDesignPattern
{
    class Program
    {
        static void Main(string[] args)
        {
            //Declare register
            IRegisterVehicule register = new RegisterVehicules();

            //Insert a few elements
            register.AddVehicule("Volkswagen", "Polo", 12300);
            register.AddVehicule("Volkswagen", "Golf GTI", 18900);
            register.AddVehicule("Volkswagen", "Passat", 27000);
            register.AddVehicule("Volkswagen", "Scirocco", 32100);
            register.AddVehicule("Volkswagen", "Touareg", 21800);

            //Get the Iterator
            IIteratorVehicule iterator = register.GetIterator();

            //While leaving elements
            while (iterator.ThereAreElements())
            {
                //Getting back the next element
                Vehicule v = iterator.Next();

                //Showing the content
                Console.WriteLine(v.Brand + " " + v.Model + "made up at " + v.FactoryDate.ToShortDateString() + " (" + v.Price + " euros)");
            }

            Console.Write("Press any key to continue...");
            Console.ReadKey(true);
        }
    }
}
