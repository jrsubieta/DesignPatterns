﻿using System;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IteratorDesignPattern
{
    public class IteratorVehicule : IIteratorVehicule
    {
        // Reference to complete list
        private ArrayList vehicules;

        // Storage index where iterator is found
        private int positionActual = -1;

        // The constructor will inyect to ArrayList in the object
        public IteratorVehicule(ArrayList listado)
        {
            this.vehicules = listado;
        }

        // Operation 1: Restart the index, placing it in the element before the first
        public void First()
        {
            this.positionActual = -1;
        }

        // Operation 2: Access to the current element
        public Vehicule Actual()
        {
            // If there is no existing elements, then it will bak null.
            // If actual index is greater than the major acceptable index, thet it will back null.
            // If actual index is -1, then it will back null.

            if ((this.vehicules == null) || (this.vehicules.Count == 0) || (positionActual > this.vehicules.Count - 1) || (this.positionActual < 0))
                return null;

            // I will return the corresponding element to actual element.
            else
                return (Vehicule)this.vehicules[positionActual];
        }

        // Operation 3: Access to the next element
        public Vehicule Next()
        {
            // If there is no existing elements, it will back null.
            // If actual index is greater than the major acceptable index, then it will back null.

            if ((this.vehicules == null) ||
                (this.vehicules.Count == 0) ||
                (positionActual + 1 > this.vehicules.Count - 1))
                return null;

            //  Index plus one is added and it wil return that element.
            else
                return (Vehicule)this.vehicules[++positionActual];
        }

        // Operation 4: Checking if there are elements in the collection
        public bool ThereAreElements()
        {
            // Method will return a boolean that will be true if the next position is less than or equal to
            // maximum acceptable index (number of elements of the array - 1).
            return (positionActual + 1 <= this.vehicules.Count - 1);
        }



    }
}
