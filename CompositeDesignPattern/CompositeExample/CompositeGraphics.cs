﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CompositeExample
{
    public class CompositeGraphics : Graphics
    {
        private List<Graphics> child = new List<Graphics>();
        public CompositeGraphics(string nombre) : base(nombre) { }
        public override void AddGraphic(Graphics graph)
        {
            child.Add(graph);
        }
        public override void DeleteGraphic(Graphics graph)
        {
            child.Remove(graph);
        }
        public override void Paint()
        {
            for (int i = 0; i < child.Count; i++)
                child[i].Paint();
        }
    }
}
