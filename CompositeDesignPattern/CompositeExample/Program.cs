
using System;
namespace CompositeExample
{
    public class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("******Painting graph 1************");
            Console.WriteLine("");
            CompositeGraphics graph1 = new CompositeGraphics("Graphic 1");
            graph1.AddGraphic(new Rectangle("Rectangle 1"));
            graph1.AddGraphic(new Triangle("Triangle 1"));
            graph1.Paint();

            Console.WriteLine("");
            Console.WriteLine("******Painting graph 2************");
            Console.WriteLine("");
            CompositeGraphics graph2 = new CompositeGraphics("Graphic 2");
            graph2.AddGraphic(new Square("Square 1"));
            graph2.AddGraphic(new Rectangle("Rectangle 2"));
            graph2.AddGraphic(graph1);
            graph1.AddGraphic(new Triangle("Triangle 2"));
            graph1.AddGraphic(new Rectangle("Square 3"));
            graph2.Paint();
            Console.Write("Press any key to continue...");
            Console.ReadKey(true);


        }
    }
}
