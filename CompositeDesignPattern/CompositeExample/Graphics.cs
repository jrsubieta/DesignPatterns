﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CompositeExample
{
    public abstract class Graphics
    {
        protected string names;
        public Graphics(string n)
        {
            names = n;
        }

        public abstract void AddGraphic(Graphics graph);
        public abstract void DeleteGraphic(Graphics graph);
        public abstract void Paint();
    }
}
