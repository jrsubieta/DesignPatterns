﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CompositeExample
{
    public class Rectangle : Graphics
    {
        public Rectangle(string name) : base(name) { }
        public override void AddGraphic(Graphics graph)
        {
            Console.WriteLine("Cannot add a new Rectangle");
        }

        public override void DeleteGraphic(Graphics graph)
        {
            Console.WriteLine("Cannot delete a Rectangle");
        }
        public override void Paint()
        {
            Console.WriteLine("##########################");
            Console.WriteLine("#                        #");
            Console.WriteLine("#                        #");
            Console.WriteLine("#                        #");
            Console.WriteLine("#                        #");
            Console.WriteLine("#                        #");
            Console.WriteLine("#                        #");
            Console.WriteLine("##########################");
        }
    }
}
