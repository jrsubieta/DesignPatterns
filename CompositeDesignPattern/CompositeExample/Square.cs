﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CompositeExample
{
    public class Square : Graphics
    { 
        public Square(string name) : base(name) { }

        public override void AddGraphic(Graphics graph)
        {
            Console.WriteLine("Cannot add a new Square");
        }

        public override void DeleteGraphic(Graphics graph)
        {
            Console.WriteLine("Cannot delete a Square");
        }

        public override void Paint()
        {
            Console.WriteLine("################");
            Console.WriteLine("#              #");
            Console.WriteLine("#              #");
            Console.WriteLine("#              #");
            Console.WriteLine("#              #");
            Console.WriteLine("#              #");
            Console.WriteLine("#              #");
            Console.WriteLine("################");
        }
    }
}