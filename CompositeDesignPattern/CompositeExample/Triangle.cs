﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CompositeExample
{
    public class Triangle : Graphics
    {
        public Triangle(string name) : base(name) { }
        public override void AddGraphic(Graphics graph)
        {
            Console.WriteLine("Cannot add a new Triangle");
        }

        public override void DeleteGraphic(Graphics graph)
        {
            Console.WriteLine("Cannot delete a Triangle");
        }
        public override void Paint()
        {
            Console.WriteLine("            #");
            Console.WriteLine("           # #");
            Console.WriteLine("          #   #");
            Console.WriteLine("         #     #");
            Console.WriteLine("        #       #");
            Console.WriteLine("       #         #");
            Console.WriteLine("      #           #");
            Console.WriteLine("     #             #");
            Console.WriteLine("    #               #");
            Console.WriteLine("   #                 #");
            Console.WriteLine("  #                   #");
            Console.WriteLine(" #                     #");
            Console.WriteLine("#########################");
        }
    }
}
