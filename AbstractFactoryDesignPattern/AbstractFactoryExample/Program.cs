using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbstractFactoryExample
{
    class Program
    {
        static void Main(string[] args)
        {
            var machine = new HotDrinkMachine();
 
            IHotDrink drink = machine.MakeDrink();
            drink.Consume();
            Console.ReadKey();
        }
    }
}
